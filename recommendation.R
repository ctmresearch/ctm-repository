library(RODBC)
library(dplyr)
library(plyr)
library(lubridate)

Get_CarrierList<-function(){
  channel <- "DRIVER={SQL Server};SERVER=ANALYTICSPROD;DATABASE=Bazooka;trusted_connection=true"  
  bazooka <- odbcDriverConnect(channel)
 
  query0<-paste0(
  "select top 20
   car.ID
  ,car.Name
  ,car.CargoLimit
  ,SUM(case when l.equipmenttype like '%V%' then 1 else 0 end ) 'DryVol'
  ,SUM(case when l.equipmenttype like 'R' then 1 else 0 end ) 'ReeferVol'
  ,COUNT(L.ID) 'TolVol'
  from bazooka.dbo.[load] L
  inner join bazooka.dbo.loadcarrier lcar on lcar.loadid = l.id and lcar.main = 1 and lcar.IsBounced = 0
  inner join Bazooka.dbo.carrier car on car.id = lcar.CarrierID
  INNER JOIN Bazooka.dbo.LoadCustomer LCUS ON LCUS.LoadID = L.ID AND LCUS.Main = 1 
  INNER JOIN Bazooka.dbo.Customer CUS ON LCUS.CustomerID = CUS.ID
  LEFT JOIN Bazooka.dbo.Customer PCUS ON CUS.ParentCustomerID = PCUS.ID
  INNER JOIN bazooka.dbo.LoadRate LR ON LR.LoadID = L.ID AND LR.EntityType = 13 AND LR.EntityID = lcar.ID
  where L.StateType = 1 and L.ProgressType =8 and l.Mode = 1 and L.LoadDate > '2018-01-01' 
  AND LCAR.CarrierID NOT IN (32936 ,244862,244863,244864,244866,244867)  AND L.ShipmentType not in (3,4,6,7)
  and (l.equipmenttype like '%V%' or l.equipmenttype like 'R')
  and car.ContractVersion NOT IN ('TMS FILE', 'UPSDS CTM', 'UPSCD CTM') and car.Name not like 'UPS%'
  AND L.TotalRAte >= 150 AND L.TotalCost >= 150
  AND  L.[OriginStateCode] in (select [Code]  FROM [Bazooka].[dbo].[State] where [ID]<=51) 
  AND  L.[DestinationStateCode] in (select [Code]  FROM [Bazooka].[dbo].[State] where [ID]<=51) 
  AND COALESCE(PCUS.CODE,CUS.CODE) NOT IN ('UPSAMZGA','UPSRAILPEA')
  and  LR.OriginalQuoteRateLineItemID = 0
  group by car.ID, car.Name
  ,car.CargoLimit
  order by 5 desc  ")
  Carrier<-sqlQuery(bazooka, query0 )
  colname(bounce_data)[1]<-'CarrierID'
  return(Carrier)
}

Recommend<-function(carrierID,DryPerc,ReeferPerc){
  carrierID=7031
  DryPerc=0.3
  ReeferPerc=0.7
  channel <- "DRIVER={SQL Server};SERVER=ANALYTICSPROD;DATABASE=Bazooka;trusted_connection=true"  
  bazooka <- odbcDriverConnect(channel)
  query0<-paste0(
  "If(OBJECT_ID('tempdb..#Carrier_HistLoad1') Is Not Null)
   Begin
  Drop Table #Carrier_HistLoad1
  End
  Create Table #Carrier_HistLoad1 (LoadID int, Insurance money, CustID int, Miles decimal(8,2), PU_Appt datetime,Origin varchar (50), Destination varchar(50), Equip varchar (20))
  Insert into #Carrier_HistLoad1
  select L.id 'LoadID',  
  CAR.cargolimit   'Insurance'
  ,CUS.ID  'CustID'
  ,Miles
  ,(case when LS.[ScheduleCloseTime] = '1753-01-01' then 
  convert(datetime, CONVERT(date, LS.LoadByDate)) + convert(datetime, CONVERT(time, LS.CloseTime)) 
  else LS.[ScheduleCloseTime] end) 'PU_Appt'
  , L.OriginCityName + ', ' + L.OriginStateCode  'Origin'
  ,L.DestinationCityName + ', ' + L.DestinationStateCode  'Destination'
  --,L.TotalValue
  ,L.EquipmentType
  FROM Bazooka.dbo.[Load] L
  INNER JOIN Bazooka.dbo.LoadCarrier LCAR ON LCAR.LoadID = L.ID and LCAR.Main = 1 and LCAR.IsBounced = 0
  INNER JOIN Bazooka.dbo.Carrier CAR ON CAR.ID = LCAR.CarrierID
  INNER JOIN Bazooka.dbo.LoadCustomer LCUS ON LCUS.LoadID = L.ID AND LCUS.Main = 1 
  INNER JOIN Bazooka.dbo.Customer CUS ON LCUS.CustomerID = CUS.ID
  LEFT JOIN Bazooka.dbo.Customer PCUS ON CUS.ParentCustomerID = PCUS.ID
  INNER JOIN bazooka.dbo.LoadRate LR ON LR.LoadID = L.ID AND LR.EntityType = 13 AND LR.EntityID = LCAR.ID and LR.OriginalQuoteRateLineItemID=0
  inner join bazooka.dbo.loadstop LS on LS.id=L.OriginLoadStopID
  WHERE L.StateType = 1
  and  L.LoadDate > '2018-01-01'  and L.Miles>0 
  AND L.Mode = 1 AND LCAR.CarrierID=",carrierID,"
  AND L.ShipmentType not in (3,4,6,7)
  AND (CASE WHEN L.EquipmentType LIKE '%V%' THEN 'V' ELSE L.EquipmentType END) IN ('V', 'R')
  AND CAR.ContractVersion NOT IN ('TMS FILE', 'UPSDS CTM', 'UPSCD CTM') --Exclude Managed Loads
  AND COALESCE(PCUS.CODE,CUS.CODE) NOT IN ('UPSAMZGA','UPSRAILPEA')
  AND L.TotalRAte >= 150 AND L.TotalCost >= 150
  AND  L.[OriginStateCode] in (select [Code]  FROM [Bazooka].[dbo].[State] where [ID]<=51) 
  AND  L.[DestinationStateCode] in (select [Code]  FROM [Bazooka].[dbo].[State] where [ID]<=51) 
  and car.Name not like 'UPS%'
  order by Origin,Destination
  
  
  
  If(OBJECT_ID('tempdb..#Carrier_HistLoad') Is Not Null)
  Begin
  Drop Table #Carrier_HistLoad
  End
  Create Table #Carrier_HistLoad (LoadID int, Insurance money, CustID int, Miles decimal(8,2), Equip varchar (20), Haul varchar(20), PU_DOW int, PU_Time varchar (20), OriginCluster varchar (50), DestinationCluster varchar (50), Corridor varchar (50))
  Insert into #Carrier_HistLoad
  
  select LoadID,Insurance,CustID,Miles,Equip
  ,case
  when Miles <250 then'Short'
  when Miles between 250 and 500 then 'Medium-Short'
  when Miles between 500 and 1000 then 'Medium'
  when Miles between 1000 and 2000 then 'Medium-Long'
  when Miles >2000 then 'Long' end 'Haul-Length'
  ,datepart(weekday,PU_Appt )  'PU_DOW'
  ,case when datepart(hour,PU_Appt) between 7 and 11 then 'Morning' when datepart(hour,PU_Appt) between 12 and 18 then 'Afternoon' 
  when datepart(hour,PU_Appt) between 19 and 23 then 'Night' else 'Dawn' end 'PU_Time'
  ,RCO.ClusterNAME 'OriginCluster'
  ,RCD.ClusterName 'DestinationCluster'
  ,RCO.ClusterNAME+'-'+RCD.ClusterName  'Corridor'
  from #Carrier_HistLoad1
  LEFT JOIN Analytics.CTM.RateClusters RCO ON RCO.Location = #Carrier_HistLoad1.Origin
  LEFT JOIN Analytics.CTM.RateClusters RCD ON RCD.Location = #Carrier_HistLoad1.Destination
  
  If(OBJECT_ID('tempdb..#Carrier_Corridor') Is Not Null)
  Begin
  Drop Table #Carrier_Corridor
  End
  Create Table #Carrier_Corridor (Corridor varchar (50), Count_Corridor int)
  Insert into #Carrier_Corridor
  select distinct corridor,
  count(loadid) 'Count_Corridor'
  from #Carrier_HistLoad
  group by Corridor
  order by 2 desc
  
  If(OBJECT_ID('tempdb..#Carrier_Origin') Is Not Null)
  Begin
  Drop Table #Carrier_Origin
  End
  Create Table #Carrier_Origin (OriginCluster varchar (50), Count_Origin int)
  Insert into #Carrier_Origin
  select distinct OriginCluster,
  count(loadid) 'Count_Origin'
  from #Carrier_HistLoad
  group by OriginCluster
  order by 2 desc
  
  If(OBJECT_ID('tempdb..#Carrier_Dest') Is Not Null)
  Begin
  Drop Table #Carrier_Dest
  End
  Create Table #Carrier_Dest (DestinationCluster varchar (50), Count_De int)
  Insert into #Carrier_Dest
  select distinct DestinationCluster,
  count(loadid) 'Count_De'
  from #Carrier_HistLoad
  group by DestinationCluster
  order by 2 desc
  
  
  
  If(OBJECT_ID('tempdb..#Carrier_Dow') Is Not Null)
  Begin
  Drop Table #Carrier_Dow
  End
  Create Table #Carrier_Dow (Dow int,PU_Time varchar (20), Count_Appt int)
  Insert into #Carrier_Dow
  select distinct PU_DOW,PU_Time,
  count(loadid) 'Count_Appt'
  from #Carrier_HistLoad
  group by PU_DOW,PU_Time
  order by 2 desc
  
  
  If(OBJECT_ID('tempdb..#CustSize') Is Not Null)
  Begin
  Drop Table #CustSize
  End
  Create Table #CustSize (CustID int, Count_ALL int)
  Insert into #CustSize
  
  select customerID,
  count(loadid)
  from bazooka.dbo.LoadCustomer LCU
  inner join bazooka.dbo.load L on L.id=LCU.LoadID
  where L.StateType = 1 and L.ProgressType =8 and l.Mode = 1 and L.LoadDate > '2018-01-01'  
  and (l.equipmenttype like '%V%' or l.equipmenttype like 'R')
  AND L.TotalRAte >= 150 AND L.TotalCost >= 150
  AND  L.[OriginStateCode] in (select [Code]  FROM [Bazooka].[dbo].[State] where [ID]<=51) 
  AND  L.[DestinationStateCode] in (select [Code]  FROM [Bazooka].[dbo].[State] where [ID]<=51) 
  and customerID in(select distinct CustID  from #Carrier_HistLoad)
  group by CustomerID
  
  If(OBJECT_ID('tempdb..#Carrier_Cust') Is Not Null)
  Begin
  Drop Table #Carrier_Cust
  End
  Create Table #Carrier_Cust (CustID int, Count_Cus int, Count_ALL int)
  Insert into #Carrier_Cust
  select distinct #Carrier_HistLoad.CustID,
  count(loadid) 'Count_Cus'
  ,#CustSize.Count_ALL 'Count_ALL'
  from #Carrier_HistLoad
  inner join #CustSize on #CustSize.CustID= #Carrier_HistLoad.CustID
  group by #Carrier_HistLoad.CustID, #CustSize.Count_ALL
  order by 2 desc")
 
  
  query1<-paste0("
  select top 5 
  X.ID, X.LoadDate,X.Appt, X.Name, X.EquipmentType,L.Miles, L.OriginCityName+','+L.OriginStateCode 'Origin', L.DestinationCityName+','+L.DestinationStateCode 'Destination',
  Count_Corridor  
  ,Ori.Count_Origin    
  , Count_Appt,Count_Cus,Count_ALL
  ,(Cor.Count_Corridor*0.3 + Ori.Count_Origin*0.2  + De.Count_De*0.2 +Count_Appt*0.1 + Count_Cus*0.1 + Count_Cus/Count_ALL*0.1)*EquipScore 'Score'
  from
  (select  
  L.id,  Loaddate,(case when LS.[ScheduleCloseTime] = '1753-01-01' then 
  convert(datetime, CONVERT(date, LS.LoadByDate)) + convert(datetime, CONVERT(time, LS.CloseTime)) 
  else LS.[ScheduleCloseTime] end) 'Appt'
  ,datepart(weekday, (case when LS.[ScheduleCloseTime] = '1753-01-01' then 
  convert(datetime, CONVERT(date, LS.LoadByDate)) + convert(datetime, CONVERT(time, LS.CloseTime)) 
  else LS.[ScheduleCloseTime] end))  'PU_DOW'
  ,case when datepart(hour,(case when LS.[ScheduleCloseTime] = '1753-01-01' then 
  convert(datetime, CONVERT(date, LS.LoadByDate)) + convert(datetime, CONVERT(time, LS.CloseTime)) 
  else LS.[ScheduleCloseTime] end))  between 7 and 11 then 'Morning'
  when datepart(hour,(case when LS.[ScheduleCloseTime] = '1753-01-01' then 
  convert(datetime, CONVERT(date, LS.LoadByDate)) + convert(datetime, CONVERT(time, LS.CloseTime)) 
  else LS.[ScheduleCloseTime] end))  between 12 and 18 then 'Afternoon'
  when datepart(hour,(case when LS.[ScheduleCloseTime] = '1753-01-01' then 
  convert(datetime, CONVERT(date, LS.LoadByDate)) + convert(datetime, CONVERT(time, LS.CloseTime)) 
  else LS.[ScheduleCloseTime] end)) between 19 and 23 then 'Night'
  else 'Dawn' end 'PU_Time'
  , CUS.Name, Cus.ID  'CustID',L.totalvalue
  ,L.EquipmentType 
  ,case when l.equipmenttype like '%V%' then ", DryPerc, " else ", ReeferPerc, " end 'EquipScore'
  ,case
  when Miles <250 then'Short'
  when Miles between 250 and 500 then 'Medium-Short'
  when Miles between 500 and 1000 then 'Medium'
  when Miles between 1000 and 2000 then 'Medium-Long'
  when Miles >2000 then 'Long' end 'Haul-Length'
  ,RCO.ClusterNAME 'OriginCluster'
  ,RCD.ClusterName 'DestinationCluster'
  ,RCO.ClusterNAME + '-'+ RCD.ClusterName 'Corridor'
  from bazooka.dbo.Load L
  INNER JOIN Bazooka.dbo.LoadCustomer LCUS ON LCUS.LoadID = L.ID AND LCUS.Main = 1 
  INNER JOIN Bazooka.dbo.Customer CUS ON LCUS.CustomerID = CUS.ID
  LEFT JOIN Bazooka.dbo.Customer PCUS ON CUS.ParentCustomerID = PCUS.ID
  LEFT JOIN Analytics.CTM.RateClusters RCO ON RCO.Location = L.OriginCityName + ', ' + L.OriginStateCode 
  LEFT JOIN Analytics.CTM.RateClusters RCD ON RCD.Location = L.DestinationCityName + ', ' + L.DestinationStateCode 
  inner join bazooka.dbo.loadstop LS on LS.id=L.OriginLoadStopID
  where L.StateType = 1 and L.ProgressType =1 and l.Mode = 1 and L.LoadDate between getdate() and dateadd (day, 7,getdate())
  AND L.ShipmentType not in (3,4,6,7)
  and (l.equipmenttype like '%V%' or l.equipmenttype like 'R')
  AND  L.[OriginStateCode] in (select [Code]  FROM [Bazooka].[dbo].[State] where [ID]<=51) 
  AND  L.[DestinationStateCode] in (select [Code]  FROM [Bazooka].[dbo].[State] where [ID]<=51) 
  AND COALESCE(PCUS.CODE,CUS.CODE) NOT IN ('UPSAMZGA','UPSRAILPEA')
  and RCO.ClusterNAME in (select OriginCluster from #Carrier_HistLoad)
  and RCD.ClusterNAME in (select DestinationCluster from #Carrier_HistLoad)
  and RCO.ClusterNAME + '-'+ RCD.ClusterName  in (select Corridor from #Carrier_HistLoad)
  and totalvalue<= (select MAX(Insurance) from #Carrier_HistLoad)      --Insurance Filter 
  and EquipmentType in (select distinct EquipmentType from #Carrier_HistLoad)      --Equipment Filter
  and CUS.ID  in (select distinct CustID  from #Carrier_HistLoad)
  )X
  left join #Carrier_Corridor Cor on Cor.Corridor=X.Corridor
  left join #Carrier_Origin Ori on Ori.OriginCluster=X.OriginCluster
  left join #Carrier_Dow Dow on Dow.Dow=X.PU_DOW  and Dow.PU_Time=X.PU_Time 
  left join #Carrier_Dest De on De.DestinationCluster=X.DestinationCluster
  left join #Carrier_Cust Cu on Cu.CustID=X.CustID
  inner join bazooka.dbo.Load L on L.id=X.ID
  order by Score desc"
  )
  sqlQuery(bazooka, query0 )
  loads<-sqlQuery(bazooka, query1 )
  colname(bounce_data)[1]<-'LoadID'
  return(loads)
  
}


carrierlist<-Get_CarrierList()

for (i in 1:nrow(carrierlist)){
  CarrierID<-carrierlist$CarrierID
  DryPerc<-carrierlist$DryVol*1.0/carrierlist$TolVol
  ReeferPerc<-carrierlist$ReeferVol*1.0/carrierlist$TolVol
  Loads<-Recommend(CarrierID,DryPerc,ReeferPerc)
  print(CarrierID)
  print(Loads)
  
}



